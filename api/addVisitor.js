'use strict'

const {
  addVisitor: _addVisitor,
} = require('../core/api-dep')

const addVisitor = async (event) => {
  try {
    const body = event.body
    const request = body.request
    const data = request.data
    const ret = await _addVisitor(data.location, data)
    return ret
  } catch (error) {
    throw error
  }
}

module.exports = {
  addVisitor,
}
